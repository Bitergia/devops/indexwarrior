#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     David Pose Fernández <dpose@bitergia.com>
#

import os
import sys
import unittest

from indexwarrior.main import IndexWarrior
from indexwarrior.command import IndexWarriorParser
from indexwarrior.errors import ParserException

if '..' not in sys.path:
    sys.path.insert(0, '..')


def read_file(filename, mode='r'):
    """Adhoc function to make reading files easier"""

    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), filename), mode) as fdesc:
        content = fdesc.read()
        return content


class TestTimeoutArgument(unittest.TestCase):
    """Test timeout argument"""

    def test_timeout_argument_default_value(self):
        """Test default value for timeout argument"""

        stdin_args = ['https://localhost:9200', 'alias', 'show']

        iwparser = IndexWarriorParser()
        args = iwparser.parse(*stdin_args)

        self.assertIsInstance(args.es_timeout, int)
        self.assertEqual(args.es_timeout, 30)

    def test_timeout_argument_custom_value(self):
        """Test custom value for timeout argument"""

        stdin_args = ['--es_timeout', '10', 'https://localhost:9200', 'alias', 'show']

        iwparser = IndexWarriorParser()
        args = iwparser.parse(*stdin_args)

        self.assertIsInstance(args.es_timeout, int)
        self.assertEqual(args.es_timeout, 10)

    def test_timeout_argument_type(self):
        """Test type of the value value for timeout argument"""

        stdin_args_1 = ['--es_timeout', '10', 'https://localhost:9200', 'alias', 'show']
        stdin_args_2 = ['--es_timeout', 'pepe', 'https://localhost:9200', 'alias', 'show']

        iwparser = IndexWarriorParser()

        args = iwparser.parse(*stdin_args_1)
        self.assertIsInstance(args.es_timeout, int)

        with self.assertRaises(ParserException) as err:
            iwparser.parse(*stdin_args_2)

        self.assertEqual(err.exception.message, "Failed to parse arguments. Check the help to know all valid arguments")
